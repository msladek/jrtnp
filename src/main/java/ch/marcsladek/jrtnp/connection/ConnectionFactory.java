package ch.marcsladek.jrtnp.connection;

import java.io.IOException;
import java.net.Socket;

public interface ConnectionFactory {

  /**
   * 
   * @param socket
   *          on which the Connection should be built
   * @return new Connection implementation for the given socket
   * @throws IOException
   *           when unable to get Streams from socket
   */
  public Connection newInstance(Socket socket) throws IOException;

}
