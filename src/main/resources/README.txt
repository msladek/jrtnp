Java Real-Time Network Protocol

JRTNP is a framework written in Java which provides real-time 
client-server network communication support for Java applications.

For more information and instructions:
http://bitbucket.org/msladek/jrtnp/wiki