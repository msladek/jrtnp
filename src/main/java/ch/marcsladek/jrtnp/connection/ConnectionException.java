package ch.marcsladek.jrtnp.connection;

public class ConnectionException extends Exception {

  private static final long serialVersionUID = 201302010437L;

  public ConnectionException() {
    super();
  }

  public ConnectionException(String message) {
    super(message);
  }

  public ConnectionException(String message, Throwable cause) {
    super(message, cause);
  }

  public ConnectionException(Throwable cause) {
    super(cause);
  }

}
