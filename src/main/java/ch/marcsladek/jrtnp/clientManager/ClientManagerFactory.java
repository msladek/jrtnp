package ch.marcsladek.jrtnp.clientManager;

import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public interface ClientManagerFactory {

  /**
   * 
   * @param connectionFactoryClass
   *          the parameter for all ClientManager
   * @return a new instance of a ClientManager
   * @throws ReflectiveOperationException
   *           when unable to use reflection on given class
   */
  public ClientManager newInstance(
      Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException;

}
