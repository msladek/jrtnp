package ch.marcsladek.jrtnp.connection;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Connection extends StreamListener {

  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");

  private final Socket socket;
  private final ObjectOutputStream out;
  private final String identifier;

  /**
   * 
   * @param socket
   *          on which the Connection should be built
   * @throws IOException
   *           when unable to get Streams from socket
   */
  protected Connection(Socket socket) throws IOException {
    super(socket.getInputStream());
    this.socket = socket;
    out = new ObjectOutputStream(socket.getOutputStream());
    identifier = getAddress();
  }

  public final String getAddress() {
    String address = "";
    InetAddress inetAddress = socket.getInetAddress();
    if (inetAddress != null) {
      address = inetAddress.getHostAddress() + ":" + socket.getPort();
    }
    return address;
  }

  public final String getLocalAddress() {
    String localAddress = "";
    InetAddress inetAddress = socket.getLocalAddress();
    if (inetAddress != null) {
      localAddress = inetAddress.getHostAddress() + ":" + socket.getLocalPort();
    }
    return localAddress;
  }

  /**
   * 
   * @return identifier String with the form 'address:port'
   */
  public final String getIdentifier() {
    return new String(identifier);
  }

  /**
   * 
   * @return a Stamp String with the format 'dd.MM.yy HH:mm:ss identifier - "
   */
  protected final String getStamp() {
    return dateFormat.format(new Date()) + " " + identifier + " - ";
  }

  /**
   * Sends an Object to the socket's OutputStream
   * 
   * @param obj
   *          Object to be sent
   * @throws IOException
   *           when unable to write Object to the Stream
   */
  public final void send(Object obj) throws IOException {
    out.writeObject(obj);
  }

  @Override
  public final void shutdown() {
    super.shutdown();
    try {
      out.close();
    } catch (Exception e) {
      // silent catch
    }
  }

  @Override
  public String toString() {
    return "Connection [id=" + getIdentifier() + ", isListening=" + isListening() + "]";
  }

}
