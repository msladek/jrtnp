package ch.marcsladek.jrtnp.connection;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.SocketException;

abstract class StreamListener extends Thread {

  private volatile boolean listening;
  private final InputStream in;

  /**
   * 
   * @param in
   *          InputStream listening to
   */
  StreamListener(InputStream in) {
    this.listening = false;
    this.in = in;
  }

  public final boolean isListening() {
    return listening;
  }

  @Override
  public final void run() {
    this.listening = true;
    boolean closedRemotely = false;
    ConnectionException closeException = new ConnectionException(
        "Finished without Exception");
    try {
      mainLoop();
    } catch (EOFException exc) {
      closeException = new ConnectionException("Connection closed by remote", exc);
      closedRemotely = true;
    } catch (SocketException exc) {
      if (exc.getMessage().equals("socket closed")) {
        closeException = new ConnectionException("Connection closed", exc);
      } else if (exc.getMessage().equals("Connection reset")) {
        closeException = new ConnectionException("Connection closed by remote", exc);
        closedRemotely = true;
      } else {
        closeException = new ConnectionException("Unknown Socket Exception", exc);
      }
    } catch (IOException exc) {
      closeException = new ConnectionException("Unknown IO Exception", exc);
    } catch (ClassNotFoundException exc) {
      closeException = new ConnectionException("Class serialisation exception", exc);
    } catch (Exception exc) {
      closeException = new ConnectionException("Unkown exception: ", exc);
    } finally {
      finish(closedRemotely, closeException);
    }
    this.listening = false;
  }

  private final void mainLoop() throws ClassNotFoundException, IOException {
    ObjectInputStream in = new ObjectInputStream(this.in);
    Object obj_in;
    try {
      while (listening && ((obj_in = in.readObject()) != null)) {
        process(obj_in);
      }
    } finally {
      shutdown();
    }
  }

  /**
   * Shuts down the StreamListener
   */
  public void shutdown() {
    this.listening = false;
    try {
      in.close();
    } catch (Exception e) {
      // silent catch
    }
  }

  /**
   * Processes the data received from the Socket's InputStream
   * 
   * This method is called when a Object has been received
   * 
   * @param obj
   *          Object received
   */
  protected abstract void process(Object obj);

  /**
   * This method is called eventually when the listener was running and is being shut
   * down.
   * 
   * @param closedRemotely
   *          is true when the connection has been closed by the other side
   * @param closeException
   *          the {@link ConnectionException} thrown for the {@link Connection} closure
   */
  protected abstract void finish(boolean closedRemotely,
      ConnectionException closeException);

}
