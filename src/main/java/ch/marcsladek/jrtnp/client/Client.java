package ch.marcsladek.jrtnp.client;

import java.io.IOException;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public abstract class Client {

  protected final Connection server;

  /**
   * 
   * @param host
   *          ip the client connects to
   * @param port
   *          port the client connects to
   * @param ssl
   *          uses Secure Sockets Layer
   * @param connectionFactoryClass
   *          class of the factory used to create objects of own {@link Connection}
   *          implementation
   * @throws ReflectiveOperationException
   *           when unable to use reflection on given class
   * @throws IOException
   *           when error with socket and stream
   */
  public Client(String host, int port, boolean ssl,
      Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException, IOException {
    ConnectionFactory factory = connectionFactoryClass.getConstructor().newInstance();
    server = factory.newInstance(getSocketFactory(ssl).createSocket(host, port));
  }

  private SocketFactory getSocketFactory(boolean ssl) {
    if (ssl) {
      return SSLSocketFactory.getDefault();
    } else {
      return SocketFactory.getDefault();
    }
  }

  /**
   * Starts the {@link Connection} for the server.
   */
  public final void start() {
    server.start();
    started();
  }

  /**
   * Is called after {@link #start()} call.
   */
  public abstract void started();

  /**
   * Stops the {@link Connection} for the server.
   */
  public final void shutdown() {
    server.shutdown();
    shuttedDown();
  }

  /**
   * Is called after {@link #shutdown()} call.
   */
  public abstract void shuttedDown();

  @Override
  public String toString() {
    return server.toString();
  }

}
