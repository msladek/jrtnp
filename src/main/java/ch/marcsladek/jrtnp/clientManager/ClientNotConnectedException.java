package ch.marcsladek.jrtnp.clientManager;

public class ClientNotConnectedException extends Exception {

  private static final long serialVersionUID = 201309242347L;

  public ClientNotConnectedException() {
    super();
  }

  public ClientNotConnectedException(String message) {
    super(message);
  }

}
