package ch.marcsladek.jrtnp.clientManager;

import java.io.IOException;
import java.net.Socket;

import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public class DefaultClientManager extends ClientManager {

  public DefaultClientManager(Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException {
    super(connectionFactoryClass);
  }

  @Override
  protected void newClientNotExistent(Connection newClient) {
    newClient.start();
  }

  @Override
  protected void newClientExistent(Connection newClient) {
    System.err.println("Connection '" + newClient + "' already exists");
  }

  @Override
  protected void newClientIOException(Socket socket, IOException exp) {
    System.err.println("Connecting to '" + socket + "' failed: " + exp.getMessage());
  }

  @Override
  protected void removed(Connection client) {
    client.shutdown();
  }

}
