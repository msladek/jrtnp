package ch.marcsladek.jrtnp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

import ch.marcsladek.jrtnp.clientManager.ClientManager;

public final class ServerSocketListener extends Thread {

  private final ServerSocket serverSocket;
  private final ClientManager clientManager;
  private final int timeout;
  private volatile boolean listening;

  ServerSocketListener(int port, ClientManager clientManager, int timeout, boolean ssl)
      throws IOException {
    serverSocket = getServerSocketFactory(ssl).createServerSocket(port);
    this.clientManager = clientManager;
    this.timeout = timeout;
    this.listening = false;
  }

  private final ServerSocketFactory getServerSocketFactory(boolean ssl) {
    if (ssl) {
      return SSLServerSocketFactory.getDefault();
    } else {
      return ServerSocketFactory.getDefault();
    }
  }

  public final boolean isListening() {
    return listening;
  }

  @Override
  public void run() {
    listening = true;
    Socket socket;
    try {
      while (listening && ((socket = serverSocket.accept()) != null)) {
        socket.setSoTimeout(timeout);
        clientManager.newClient(socket);
      }
    } catch (IOException e) {
    } finally {
      listening = false;
    }
  }

  final void shutdown() {
    listening = false;
    try {
      serverSocket.close();
    } catch (Exception e) {
      // silent close
    }
  }

  final int getPort() {
    return serverSocket.getLocalPort();
  }

  @Override
  public String toString() {
    return "ServerSocketListener [serverSocket=" + serverSocket + ", isListening=" + isListening()
        + ", clientManager=" + clientManager + "]";
  }

}
