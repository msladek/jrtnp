package ch.marcsladek.jrtnp.clientManager;

import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public abstract class ClientManager {

  protected final ConcurrentHashMap<String, Connection> clientMap;
  protected final ConnectionFactory factory;

  /**
   * @param connectionFactoryClass
   *          class of the factory used to create objects of own Connection implementation
   * @throws ReflectiveOperationException
   *           when unable to use reflection on given class
   */
  protected ClientManager(Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException {
    clientMap = new ConcurrentHashMap<String, Connection>();
    factory = connectionFactoryClass.getConstructor().newInstance();
  }

  /**
   * Returns and starts a new Connection for the given Socket and adds it to the pool
   * 
   * @param socket
   *          for establishing the connection to
   * @throws IOException
   *           when unable to listen to socket
   */
  public final void newClient(Socket socket) {
    try {
      Connection newClient = factory.newInstance(socket);
      if (clientMap.putIfAbsent(newClient.getIdentifier(), newClient) == null) {
        newClientNotExistent(newClient);
      } else {
        newClientExistent(newClient);
      }
    } catch (IOException exp) {
      newClientIOException(socket, exp);
    }
  }

  /**
   * is called when new Connection is not existent
   * 
   * @param newClient
   */
  protected abstract void newClientNotExistent(Connection newClient);

  /**
   * is called when new Connection is already existent
   * 
   * @param newClient
   */
  protected abstract void newClientExistent(Connection newClient);

  /**
   * is called when IOException has occured while creating Connection for the socket provided
   * 
   * @param socket
   * @param exp
   */
  protected abstract void newClientIOException(Socket socket, IOException exp);

  /**
   * Removes the given Connection from the pool
   * 
   * @param identifier
   *          specifies Connection
   * @return removed Connection if successful
   * @throws ClientNotConnectedException
   *           is thrown when given Connection is not connected
   */
  public final Connection removeClient(String identifier) throws ClientNotConnectedException {
    Connection client = clientMap.remove(identifier);
    if (client != null) {
      removed(client);
      return client;
    } else {
      throw new ClientNotConnectedException(identifier);
    }
  }

  /**
   * is called when new Connection has been successfully removed
   * 
   * @param client
   */
  protected abstract void removed(Connection client);

  /**
   * Sends the Object to the Connection given connection
   * 
   * @param identifier
   *          specifies Connection
   * @param obj
   *          Object being sent
   * @return whether was successful or not
   * @throws IOException
   *           when unable to access socket
   * @throws ClientNotConnectedException
   *           is thrown when given Connection is not connected
   */
  public final void send(String identifier, Object obj) throws IOException,
      ClientNotConnectedException {
    Connection client = clientMap.get(identifier);
    if (client != null) {
      client.send(obj);
    } else {
      throw new ClientNotConnectedException(identifier);
    }
  }

  /**
   * @param identifier
   *          specifies Connection
   * @return address of specified Connection
   * @throws ClientNotConnectedException
   *           is thrown when given Connection is not connected
   */
  public final String getAddressOf(String identifier) throws ClientNotConnectedException {
    Connection client = clientMap.get(identifier);
    if (client != null) {
      return client.getAddress();
    } else {
      throw new ClientNotConnectedException(identifier);
    }
  }

  /**
   * @param identifier
   *          specifies Connection
   * @return local address of specified Connection
   * @throws ClientNotConnectedException
   *           is thrown when given Connection is not connected
   */
  public final String getLocalAddressOf(String identifier) throws ClientNotConnectedException {
    Connection client = clientMap.get(identifier);
    if (client != null) {
      return client.getLocalAddress();
    } else {
      throw new ClientNotConnectedException(identifier);
    }
  }

  /**
   * @return a list of connected Clients
   */
  public final Collection<String> list() {
    return clientMap.keySet();
  }

  /**
   * shuts down the connections to all clients
   */
  public final void shutdown() {
    for (Connection client : clientMap.values()) {
      client.shutdown();
    }
  }

  @Override
  public String toString() {
    return "ClientManager [connectionFactory=" + factory.getClass().getName() + ", clientMap="
        + clientMap + "]";
  }

}
