package ch.marcsladek.jrtnp.server;

import java.io.IOException;

import ch.marcsladek.jrtnp.clientManager.ClientManager;
import ch.marcsladek.jrtnp.clientManager.ClientManagerFactory;
import ch.marcsladek.jrtnp.clientManager.DefaultClientManager;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public abstract class Server {

  protected final ClientManager clientManager;
  protected final ServerSocketListener serverSocketListener;

  /**
   * 
   * @param port
   *          number the server is listening to
   * @param timeout
   *          timeout for clients
   * @param ssl
   *          uses Secure Sockets Layer
   * @param connectionFactoryClass
   *          class of the factory used to create objects of own {@link Connection} implementation
   * @throws ReflectiveOperationException
   *           when unable to use reflection on given class
   * @throws IOException
   *           when unable to start up {@link ServerSocketListener}
   */
  public Server(int port, int timeout, boolean ssl,
      Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException, IOException {
    this(port, timeout, ssl, connectionFactoryClass, null);
  }

  /**
   * 
   * @param port
   *          number the server is listening to
   * @param timeout
   *          timeout for clients
   * @param ssl
   *          uses Secure Sockets Layer
   * @param connectionFactoryClass
   *          class of the factory used to create objects of own {@link Connection} implementation
   * @param clientManagerFactoryClass
   *          class of the factory used to create objects of own {@link ClientManager}
   *          implementation
   * @throws ReflectiveOperationException
   *           when unable to use reflection on given class
   * @throws IOException
   *           when unable to start up {@link ServerSocketListener}
   */
  public Server(int port, int timeout, boolean ssl,
      Class<? extends ConnectionFactory> connectionFactoryClass,
      Class<? extends ClientManagerFactory> clientManagerFactoryClass)
      throws ReflectiveOperationException, IOException {
    clientManager = getClientManager(connectionFactoryClass, clientManagerFactoryClass);
    serverSocketListener = new ServerSocketListener(port, clientManager, timeout, ssl);
  }

  private ClientManager getClientManager(Class<? extends ConnectionFactory> connectionFactoryClass,
      Class<? extends ClientManagerFactory> clientManagerFactoryClass)
      throws ReflectiveOperationException {
    if (clientManagerFactoryClass != null) {
      ClientManagerFactory factory = clientManagerFactoryClass.getConstructor().newInstance();
      return factory.newInstance(connectionFactoryClass);
    } else {
      return new DefaultClientManager(connectionFactoryClass);
    }
  }

  /**
   * Starts the underlying {@link ServerSocketListener}.
   */
  public final void start() {
    serverSocketListener.start();
    started();
  }

  /**
   * Is called after {@link #start()} call.
   */
  public abstract void started();

  /**
   * Stops the underlying {@link ServerSocketListener} and shuts down the {@link Connection}s to all
   * the clients.
   */
  public final void shutdown() {
    serverSocketListener.shutdown();
    clientManager.shutdown();
    shuttedDown();
  }

  /**
   * Is called after {@link #shutdown()} call.
   */
  public abstract void shuttedDown();

  /**
   * @return the port the {@link ServerSocketListener} is listening to
   */
  public int getPort() {
    return serverSocketListener.getPort();
  }

  @Override
  public String toString() {
    return "Server [serverSocketListener=" + serverSocketListener + ", clientManager="
        + clientManager + "]";
  }

}
